package org.vga.ar;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SaludadorTest {

    private static Saludador saludador;

    @BeforeAll
    static void setUp(){
        saludador = new Saludador();
    }

    @Test
    void saludarSaludaBien() {
        String saludo = saludador.saludar("Vladimiro");
        assertEquals(saludo, "Hola " + "Vladimiro");
    }
}